/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
  * Morse Code Master
  * Thomas Zahner
  * thomas@zahnerconsulting.com
**/

'use strict';

const Alexa = require('alexa-sdk');

const APP_ID = '';

const languageStrings = {
	'en': {
		translation: {
			SKILL_NAME: 'Morse Code Master',
			HELP_MESSAGE: 'I can help learn and practice Morse Code. Please say, give me a quiz.',
			HELP_REPROMPT: 'What can I help you with?',
			STOP_MESSAGE: 'Goodbye!',
			UHANDLED_MESSAGE: 'I\'m sorry, but something went wrong.',
		},
	,
};

const handlers = {
	'LaunchRequest': function () {
		console.log('In: LaunchRequest');
	},
	'NewSession': function () {
		console.log('In: NewSession');
	},
	'Unhandled': function () {
		console.log('In: Unhandled');
		this.emit(':tell', this.t('UHANDLED_MESSAGE'));
	},
	'GiveQuizIntent': function () {
		console.log('In: GiveQuiz');
		console.log(this.event);

		this.emit(':tellWithCard', "OK, here is your quiz.", this.t('SKILL_NAME'), "Quiz:");
	},
	'AMAZON.HelpIntent': function () {
		const speechOutput = this.t('HELP_MESSAGE');
		const reprompt = this.t('HELP_MESSAGE');
		this.emit(':ask', speechOutput, reprompt);
	},
	'AMAZON.CancelIntent': function () {
		this.emit(':tell', this.t('STOP_MESSAGE'));
	},
	'AMAZON.StopIntent': function () {
		this.emit(':tell', this.t('STOP_MESSAGE'));
	},
};

exports.handler = function (event, context) {
    const alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.resources = languageStrings;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

